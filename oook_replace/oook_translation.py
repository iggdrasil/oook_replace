#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.utils import translation
from django.utils.translation import pgettext_lazy

class OOOTranslation(object):
    initialized = False
    oook_translation = {}

    @classmethod
    def get_oook_translation(cls):
        if cls.initialized:
            return cls.oook_translation
        TRANSLATION_STRINGS = []
        # dynamic load of TRANSLATION_STRINGS in other apps
        for app in settings.INSTALLED_APPS:
            # don't load current app
            if app == 'oook_replace':
                continue
            capp = __import__(app)
            if hasattr(capp, 'TRANSLATION_STRINGS'):
                TRANSLATION_STRINGS += capp.TRANSLATION_STRINGS
        cur_language = translation.get_language()

        try:
            for language in settings.LANGUAGES:
                translation.activate(language)
                cls.oook_translation[language] = {}
                for k, v in TRANSLATION_STRINGS:
                    cls.oook_translation[language][k] = unicode(v)
        finally:
            translation.activate(cur_language)
        cls.initialized = True
        return cls.oook_translation

